import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

var firebaseConfig = {
  apiKey: "AIzaSyA2vwiuT0aAzdqfWJsls9PJ-JIQ3Whr854",
  authDomain: "molla-project.firebaseapp.com",
  databaseURL: "https://molla-project.firebaseio.com",
  projectId: "molla-project",
  storageBucket: "molla-project.appspot.com",
  messagingSenderId: "868022771410",
  appId: "1:868022771410:web:b377995cb35311de10570b",
  measurementId: "G-YCM2QSB2DN"
};
// Initialize Firebase
class FirebaseClass {
  constructor() {
    app.initializeApp(firebaseConfig);
    this.auth = app.auth();
    this.store = app.firestore();
  }
  // authentication
  createUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  signInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignOut = () => this.auth.signOut();

  doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = password => this.auth.currentUser.updatePassword(password);

  // store
  addDocument = (collectionName, data) =>
    this.store.collection(collectionName).add(data);

  getAllItems = collectionName => this.store.collection(collectionName).get();

  deleteDocument = (collectionName, doc_id) =>
    this.store
      .collection(collectionName)
      .doc(doc_id)
      .delete();
}

export default new FirebaseClass();

//   export default !app.apps.length ? app.initializeApp(firebaseConfig) : app.app();
