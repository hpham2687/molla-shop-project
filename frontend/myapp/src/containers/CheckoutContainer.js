import React, { useContext, useEffect } from "react";
import Header from "../components/Header";
import MainCheckout from "../components/CheckoutComponents";
import Footer from "../components/Footer";
import * as ContextApi from "../context";
import { FirebaseContext } from "../firebase";

import ModalMolla from "../components/ModalMolla";

const CheckoutContainer = () => {
  const {
    ProductDataContexts,
    UserContexts,
    UiContexts,
    CartContexts
  } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const firebaseContext = useContext(FirebaseContext);
  const uiContexts = useContext(UiContexts);
  const userContexts = useContext(UserContexts);
  const cartContexts = useContext(CartContexts);


  return (
    <>
      {ProductDataContexts.productData ? ProductDataContexts.productData : null}
      <ModalMolla
        firebase={firebaseContext}
        userContexts={userContexts}
        uiContexts={uiContexts}
        changeStatus={uiContexts.UiDispatch}
      />

      <Header
        firebase={firebaseContext}
        userContexts={userContexts}
        uiContexts={uiContexts}
        cartContexts={cartContexts}
      />
      <MainCheckout
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        productDataContext={productDataContext}
      />
      <Footer />
    </>
  );
};

export default CheckoutContainer;
