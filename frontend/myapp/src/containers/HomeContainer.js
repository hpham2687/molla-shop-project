import React, { useContext, useEffect } from "react";
import Header from "../components/Header";
import MainHome from "../components/HomeComponents";
import Footer from "../components/Footer";
import * as ContextApi from "../context";
import { FirebaseContext } from "../firebase";

import ModalMolla from "../components/ModalMolla";

const HomeContainer = () => {
  const {
    ProductDataContexts,
    UserContexts,
    UiContexts,
    CartContexts
  } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const firebaseContext = useContext(FirebaseContext);
  const uiContexts = useContext(UiContexts);
  const userContexts = useContext(UserContexts);
  const cartContexts = useContext(CartContexts);

  const getAllProducts = () => {
    firebaseContext.getAllItems("productsData").then(querySnapshot => {
      const a = [];
      querySnapshot.forEach(doc => {
        // console.log(`${doc.id} => ${doc.data()}`);
        a.push({
          doc_id: doc.id,
          doc_data: doc.data()
        });
      });

      productDataContext.productDataDispatch({ type: "SET", payload: a });
    });
  };

  useEffect(() => {
    // get list product
    getAllProducts();
  }, []);

  return (
    <>
      {ProductDataContexts.productData ? ProductDataContexts.productData : null}
      <ModalMolla
        firebase={firebaseContext}
        userContexts={userContexts}
        uiContexts={uiContexts}
        changeStatus={uiContexts.UiDispatch}
      />

      <Header
        firebase={firebaseContext}
        userContexts={userContexts}
        uiContexts={uiContexts}
        cartContexts={cartContexts}
      />
      <MainHome
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        productDataContext={productDataContext}
      />
      <Footer />
    </>
  );
};

export default HomeContainer;
