import React from "react";

const AuthUserContext = React.createContext(null);

const CartContexts = React.createContext();
const ProductDataContexts = React.createContext();
const UserContexts = React.createContext();
const UiContexts = React.createContext();

export {
  AuthUserContext,
  CartContexts,
  ProductDataContexts,
  UserContexts,
  UiContexts
};
