const initialUserState = {
  authUser: null
};

export const cartReducer = (state, action) => {
  switch (action.type) {
    case "SET":
      let isExist = false;
      const cloneState = [...state];
      cloneState.forEach((value, index) => {
        if (value.doc_id == action.payload.doc_id) {
          console.log("trung");
          isExist = true;
          action.payload.count = ++value.count;
          cloneState.splice(index, 1); // xóa 1 phần tử từ vị trí 2
        }
      });
      if (!isExist) action.payload.count = 1;

      cloneState.push(action.payload);
      console.log(action.payload);
      return cloneState;
    case "DELETE":
      const newState = [...state];
      var filtered = newState.filter(
        (value, index) => value.doc_id != action.payload
      );
      return filtered;
    default:
      return state;
  }
};

export const userReducer = (state, action) => {
  switch (action.type) {
    case "SET":
      return { ...state, authUser: action.payload };
    case "reset":
      return initialUserState;
    default:
      return state;
  }
};

const initialProductDataState = [];
export const productDataReducer = (state, action) => {
  switch (action.type) {
    case "SET":
      return action.payload;
    case "reset":
      return initialProductDataState;
    default:
      return state;
  }
};

export const uiReducer = (state, action) => {
  switch (action.type) {
    case "SET_MODAL_STATUS":
      return { ...state, isShowModalAuth: action.status };
    case "SET_TRENDY_PRODUCT_TAB":
      return { ...state, trendyProductTab: action.payload };
    case "SET_RECENT_ARRIVAL_PRODUCT_TAB":
      return { ...state, recentArrivalProductTab: action.payload };

    default:
      return state;
  }
};
