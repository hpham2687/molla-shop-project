import React, { Component, useContext } from "react";
import "./../assets/css/Admin.css";
import { FirebaseContext } from "../firebase";

const Admin = () => {
  const firebaseContext = useContext(FirebaseContext);
  return (
    <div>
      <AdminForm firebase={firebaseContext} />
    </div>
  );
};

class AdminForm extends Component {
  state = {
    name: "name",
    price: "123",
    description: "Sed egestas, ante et vulputate volutpat,",
    image_url: [],
    category: "women",
    reviews: [
      {
        star: null,
        reviewerName: "Samanta J.",
        reviewerTime: "",
        reviewTitle: "Good, perfect size",
        reviewDetail: "lor sit amet, consectetur a"
      }
    ],
    trendy: true,
    newReview: {
      star: null,
      reviewerName: null,
      reviewerTime: "",
      reviewTitle: null,
      reviewDetail: null
    },
    listProducts: []
  };
  getAllProducts() {
    this.props.firebase.getAllItems("productsData").then(querySnapshot => {
      let a = [];
      querySnapshot.forEach(doc => {
        console.log(`${doc.id} => ${doc.data()}`);
        a.push({
          doc_id: doc.id,
          doc_data: doc.data()
        });
      });
      console.log(a);
      this.setState({ listProducts: a });
    });
  }
  componentDidMount() {
    this.getAllProducts();
  }
  onChange = event => {
    let name = event.currentTarget.dataset.name;
    let value = event.currentTarget.value;
    if (name == "image_url" || name == "category") {
      let newValue = value.split(",");

      this.setState({ [name]: newValue });
    } else {
      this.setState({ [name]: value });
    }
    console.log(value);
  };
  onChangeReview = event => {
    let name = event.currentTarget.dataset.name;
    let value = event.currentTarget.value;
    let newDataReview = { ...this.state.newReview, [name]: value };
    this.setState({ ...this.state, newReview: newDataReview });
  };
  handleAddNewReview = event => {
    let currentReviews = [...this.state.reviews];
    console.log(currentReviews);
    currentReviews.push(this.state.newReview);
    console.log(currentReviews);

    this.setState({ ...this.state, reviews: currentReviews });
  };
  onSubmit = () => {
    let document2Add = { ...this.state };
    document2Add.star = Math.floor(Math.random() * 5) + 1; // returns a random integer from 1 to 10
    document2Add.description =
      "que laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptat";
    document2Add.dateArrvial = Math.floor(Date.now() / 1000);
    delete document2Add.newReview;
    delete document2Add.listProducts;

    this.props.firebase
      .addDocument("productsData", document2Add)
      .then(function(docRef) {
        console.log(
          "productsData" + "<- Document written with ID: ",
          docRef.id
        );
      })
      .catch(function(error) {
        console.error("Error adding document: ", error);
      });
  };
  handleDelete = e => {
    let id2Delete = e.currentTarget.dataset.id;

    this.props.firebase
      .deleteDocument("productsData", id2Delete)
      .then(function() {
        console.log("Document successfully deleted!");
      })
      .catch(function(error) {
        console.error("Error removing document: ", error);
      });
    this.getAllProducts();
  };
  render() {
    let {
      name,
      price,
      description,
      image_url,
      category,
      newReview,
      listProducts
    } = this.state;

    let { reviewerName, reviewTitle, reviewDetail, star } = newReview;
    return (
      <div className="admin">
        <div class="form">
          <label>name:</label>
          <br />
          <input
            onChange={this.onChange}
            type="text"
            data-name="name"
            value={name}
          />
          <br />

          <label>price:</label>
          <br />
          <input
            onChange={this.onChange}
            type="text"
            data-name="price"
            value={price}
          />
          <br />

          <label>description:</label>
          <br />
          <input
            onChange={this.onChange}
            type="text"
            data-name="description"
            value={description}
          />
          <br />

          <label>image_url: comma ; to separate</label>
          <br />
          <input
            onChange={this.onChange}
            type="text"
            data-name="image_url"
            value={image_url}
          />
          <br />

          <label>category:</label>
          <br />
          <input
            onChange={this.onChange}
            type="text"
            data-name="category"
            value={category}
          />
          <br />

          <label>reviews:</label>
          <br />
          <div class="a-review">
            <label>star:</label>
            <br />
            <input
              onChange={this.onChangeReview}
              type="text"
              data-name="star"
              value={star}
            />
            <br />
            <label>reviewerName:</label>
            <br />
            <input
              onChange={this.onChangeReview}
              type="text"
              data-name="reviewerName"
              value={reviewerName}
            />
            <br />
            <label>reviewTitle:</label>
            <br />
            <input
              onChange={this.onChangeReview}
              type="text"
              data-name="reviewTitle"
              value={reviewTitle}
            />
            <br />
            <label>reviewDetail:</label>
            <br />
            <input
              onChange={this.onChangeReview}
              type="text"
              data-name="reviewDetail"
              value={reviewDetail}
            />
            <br />

            <button onClick={this.handleAddNewReview}>+ add reviews</button>
          </div>

          <br />

          <input onClick={this.onSubmit} type="submit" defaultValue="Submit" />
        </div>
        <div class="preview">
          <ul>
            {listProducts.map((value, index) => {
              return (
                <>
                  <li key={index}>
                    <span>{value.doc_id}</span>
                    {value.doc_data.name}
                    <span
                      data-id={value.doc_id}
                      onClick={e => this.handleDelete(e)}
                    >
                      X
                    </span>
                  </li>
                </>
              );
            })}
          </ul>
        </div>

        {JSON.stringify(this.state)}
      </div>
    );
  }
}

export default Admin;
