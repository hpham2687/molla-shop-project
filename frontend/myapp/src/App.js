import React, { useReducer, useEffect, useContext } from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./assets/css/custom.css";
import "./assets/css/style.css";
import "./assets/css/responsive.css";
import "react-toastify/dist/ReactToastify.css";

import { ToastContainer } from "react-toastify";
import * as ContextApi from "./context";
import * as reducer from "./reducers";

import HomePage from "./pages/HomePage";
import SignUpPage from "./pages/SignUp";
import AdminPage from "./pages/Admin";
import CheckOutPage from "./pages/CheckOutPage";


import Firebase, { FirebaseContext } from "./firebase";
import { showNotification } from "./components/Notification";

const initialUserState = {
  authUser: null
};
const initialProductDataState = [];
const initialUiState = {
  isShowModalAuth: false,
  trendyProductTab: "All",
  recentArrivalProductTab: "All"
};

const initialCartState = [];

function App() {
  const {
    ProductDataContexts,
    UserContexts,
    UiContexts,
    CartContexts
  } = ContextApi;
  const [user, userDispatch] = useReducer(
    reducer.userReducer,
    initialUserState
  );
  const [productData, productDataDispatch] = useReducer(
    reducer.productDataReducer,
    initialProductDataState
  );
  const [ui, UiDispatch] = useReducer(reducer.uiReducer, initialUiState);

  const [cart, cartDispatch] = useReducer(
    reducer.cartReducer,
    initialCartState
  );

  useEffect(() => {
    const abc = Firebase.auth.onAuthStateChanged(authUser => {
      if (authUser) {
        userDispatch({ type: "SET", payload: authUser });
        UiDispatch({ type: "SET_MODAL_STATUS", status: false });
      } else {
        userDispatch({ type: "SET", payload: null });
      }
    });
    return () => abc();
  }, []);

  return (
    <>
      <ToastContainer />

      <ProductDataContexts.Provider
        value={{
          productData,
          productDataDispatch
        }}
      >
        <UserContexts.Provider value={{ user, userDispatch }}>
          <UiContexts.Provider value={{ ui, UiDispatch }}>
            <CartContexts.Provider value={{ cart, cartDispatch }}>
              <FirebaseContext.Provider value={Firebase}>
                <main className="main">
                  <Router>
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/checkout" component={CheckOutPage} />

                    <Route exact path="/signup" component={SignUpPage} />
                    <Route exact path="/admin" component={AdminPage} />
                  </Router>
                </main>
              </FirebaseContext.Provider>
            </CartContexts.Provider>
          </UiContexts.Provider>
        </UserContexts.Provider>
      </ProductDataContexts.Provider>
    </>
  );
}

export default App;
