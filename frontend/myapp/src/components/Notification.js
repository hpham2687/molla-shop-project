import { toast } from "react-toastify";

export const NOTIFICATIONS_TYPES = {
  SUCCESS: "success",
  ERROR: "error"
};
const optionss = {
  autoClose: 6000,
  type: toast.TYPE.INFO,
  hideProgressBar: false,
  position: toast.POSITION.TOP_LEFT,
  pauseOnHover: true,
  className: "toast-custom"
  // and so on ...
};

export const showNotification = (
  type,
  content = "default",
  options = optionss
) => {
  switch (type) {
    case NOTIFICATIONS_TYPES.SUCCESS:
      toast.success(content, options);
      break;

    case NOTIFICATIONS_TYPES.ERROR:
      toast.error(content, options);
      break;
  }
};
