import React, { useState } from 'react';
import { convert2SmallImg } from "../../helpers";
import { showNotification } from "./../../components/Notification";

const MainCheckout = (props) => {
	var [shipFee, setShipFee] = useState(0);

	var subTotal = 0;
	if (props.cartContexts.cart.length>0)
	props.cartContexts.cart.forEach((value, index)=> {
		
		subTotal += value.count* value.doc_data.price
	})

	console.log(subTotal);
	const handleDeleteCartItem = (doc_id) => {
		props.cartContexts.cartDispatch({
		   type: "DELETE",
		   payload: doc_id
		 })
				 showNotification("success", "Log out successfully!");

   }
	console.log(props.cartContexts.cart);

	const handleChangeShipType = (e) => {
		let shipType = e.currentTarget.dataset.name
		switch (shipType){
			case 'free-shipping': setShipFee(0); return;
			case 'standard-shipping': setShipFee(10); return;
			case 'express-shipping': setShipFee(20); return;

		}
		
	}

	const RenderCartList = () => {
		if (props.cartContexts.cart.length>0)
		return props.cartContexts.cart.map((value, index) => {
			const { doc_id } = value;
			const { doc_data } = value;
			return (
				<> 
					<tr>
			<td className="product-col">
				<div className="product">
					<figure className="product-media">
							<img src={convert2SmallImg(doc_data.image_url[0])} alt="Product" />
						
					</figure>
					<h3 className="product-title">
					{doc_data.name}
					</h3>
				</div>
			</td>
			<td className="price-col">${doc_data.price}</td>
			<td className="quantity-col">

				<div className="cart-product-quantity-btn">
						<div className="input-group-prepend">
							<button style={{minWidth: '26px'}} className="btn btn-decrement btn-spinner" type="button">
								<i class="fas fa-minus"></i>
							</button>
						</div>
						<input type="text" style={{textAlign: 'center'}} className="form-control " value={value.count} required placeholder />
						<div className="input-group-append">
							<button style={{minWidth: '26px'}} className="btn btn-increment btn-spinner" type="button">
								<i class="fas fa-plus"></i>


							</button>
						</div>
					
				</div>


			</td>
			<td className="total-col">${value.count* doc_data.price}</td>
			<td   onClick={(e) => handleDeleteCartItem(doc_id)} className="remove-col">
				<i class="fal fa-times"></i>
			</td>
		</tr>

				</>
			)
		})
		else return <tr style={{border:'none'}}><td>No Products in Cart!</td><td></td><td></td><td></td><td></td></tr>
	}
    return (
        <div class="checkout-containter">
            <div class="page-title-banner">
                <h3 class="page-title-banner___title">Shopping Cart</h3>
                <span class="page-title-banner___subtitle">SHOP</span>
            </div>

            <div class="checkout-content-wrapper">
                <div class="grid wide">
                    <div class="row">
                        <div class="col l-8 m-6 c-12">
                            
      
<table className="table-cart">
	<thead>
		<tr>
			<th>Product</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Total</th>
			<th />
		</tr>
	</thead>
	<tbody>
	
	<RenderCartList/>
	</tbody>
</table>
                        </div>
                        <div class="col l-4 m-6 c-12">
                            <div className="cart-overview">
								<h3 className="cart-overview__heading">Cart Total</h3>
								
										<table className="cart-overview__table">
										<tr>
											<th>
											Subtotal:</th>
											<th>${subTotal}</th>
										</tr>
										<tr>
											<th>
											Shipping:</th>
											<th></th>
										</tr>
										<tr>
											<td><input onChange={handleChangeShipType} name="shipping" type="radio" data-name="free-shipping"  checked={shipFee==0 ? "true" :false} class="custom-control-input"/>Free shipping:</td>
											<td>	$0.00</td>
										</tr>
										<tr>
											<td><input  onChange={handleChangeShipType} name="shipping" type="radio" data-name="standard-shipping"  checked={shipFee==10 ? "true" :false} class="custom-control-input" />Standard:</td>
											<td>	$10.00</td>
										</tr>
										<tr>
											<td><input  onChange={handleChangeShipType} name="shipping" type="radio" data-name="express-shipping"  checked={shipFee==20 ? "true" :false} class="custom-control-input" />Express:</td>
											<td>	$20.00</td>
										</tr>
										</table>

						<div className="checkout-btn-group">
							<div className="total-price">
								<span>Total</span>
								<span>${subTotal+shipFee}</span>
								
							</div>
							<div className="carousel__btn btn-outline-orange checkout-btn">PROCEED TO CHECK OUT<i class="fa fa-caret-right"></i></div>
						</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MainCheckout;