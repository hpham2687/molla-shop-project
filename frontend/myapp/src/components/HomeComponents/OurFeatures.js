import React from "react";

const OurFeatures = () => {
  return (
    <>
      <div className="our-features">
        <div className="grid wide">
          <div className="row">
            <div className="col l-4 m-4 c-12">
              <div className="our-features-item">
                <h3 className="our-features-item__title">
                  <i className="far fa-rocket" />
                  <br />
                  Payment &amp; Delivery
                </h3>
                <span className="our-features-item__description">
                  Free shipping for orders over $50
                </span>
              </div>
            </div>
            <div className="col l-4 m-4 c-12">
              <div className="our-features-item">
                <h3 className="our-features-item__title">
                  <i className="far fa-undo" />
                  <br />
                  Return &amp; Refund
                </h3>
                <span className="our-features-item__description">
                  Free 100% money back guarantee
                </span>
              </div>
            </div>
            <div className="col l-4 m-4 c-12">
              <div className="our-features-item">
                <h3 className="our-features-item__title">
                  <i className="far fa-life-ring" />
                  <br />
                  Payment &amp; Delivery
                </h3>
                <span className="our-features-item__description">
                  Alway online feedback 24/7
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OurFeatures;
