import React from "react";

const ListBlog = () => {
  return (
    <>
      <div className="intro blog">
        <div className="grid wide">
          <h1 className="trendy-product__title">From Our Blog </h1>
          <div className="row">
            <div className="col l-4 m-4 c-12">
              <div className="blog-item">
                <img
                  src="https://d-themes.com/react/molla/assets/images/demos/demo-2/blog/post-1.jpg"
                  alt=""
                  srcSet
                />
                <p className="time_stamp">Dec 12, 2018, 0 comments</p>
                <h3 className="blog-item__title">Sed adipiscing ornare</h3>
                <p className="blog-item__description">
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Phasellus hendrerit. Pellentesque aliqu
                </p>
              </div>
            </div>
            <div className="col l-4 m-4 c-12">
              <div className="blog-item">
                <img
                  src="https://d-themes.com/react/molla/assets/images/demos/demo-2/blog/post-2.jpg"
                  alt=""
                  srcSet
                />
                <p className="time_stamp">Dec 12, 2018, 0 comments</p>
                <h3 className="blog-item__title">Fusce lacinia arcuet nulla</h3>
                <p className="blog-item__description">
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Phasellus hendrerit. Pellentesque aliqu
                </p>
              </div>
            </div>
            <div className="col l-4 m-4 c-12">
              <div className="blog-item">
                <img
                  src="https://d-themes.com/react/molla/assets/images/demos/demo-2/blog/post-3.jpg"
                  alt=""
                  srcSet
                />
                <p className="time_stamp">Dec 12, 2018, 0 comments</p>
                <h3 className="blog-item__title">
                  Quisque volutpat mattis eros
                </h3>
                <p className="blog-item__description">
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Phasellus hendrerit. Pellentesque aliqu
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col l-12 m-6 c-12">
              <div className="more-container">
                <div className="load-more-btn btn-white view-article">
                  View more articles <i className="fa fa-long-arrow-down" />
                  <i className="fas fa-spinner unactive" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ListBlog;
