import React from "react";
import { showNotification } from "../Notification";
import { convert2SmallImg, filterProduct } from "../../helpers";

const TrendyProduct = props => {
  const RenderStar = ({ numStar }) => {
    let a = [];
    for (var i = 0; i < 5; i++) {
      if (i <= numStar) a.push(<i className="fa fa-star active-star" />);
      else a.push(<i className="fa fa-star" />);
    }
    return a;
  };
  const handleClickTab = e => {
    console.log(e.currentTarget.dataset.name);
    props.uiContexts.UiDispatch({
      type: "SET_TRENDY_PRODUCT_TAB",
      payload: e.currentTarget.dataset.name
    });
  };
  // if (props.ProductDataContexts.productData)
  const activeTab = props.uiContexts.ui.trendyProductTab;

  const handleAddToCart = ({ doc_id, doc_data }) => {
    // console.log(doc_id, doc_data)
    props.cartContexts.cartDispatch({
      type: "SET",
      payload: { doc_id, doc_data }
    });
    showNotification("success", "Item added to card!");
  };
  const ListTrendyProducts = () => {
    if (props.productDataContext.productData.length > 0)
      return filterProduct(props.productDataContext.productData, [
        activeTab,
        "trendy"
      ]).map((value, index) => {
        if (index > 4) return null; // chỉ hiện thị 4 phần tử
        const { doc_id } = value;
        const { doc_data } = value;
        const numReview = doc_data.reviews.length || 0;
        const numStar = Math.floor(
          doc_data.reviews.reduce(function(acc, val) {
            return acc + val.count || 2;
          }, 0) / numReview
        );

        // console.log('vaodda');

        return (
          <>
            <div key={index} className="col l-3 m-4 c-6">
              <div className="product-item">
                <div className="product-label product-label--red">NEW</div>
                <div className="add-to-wishlish-icon">
                  <i className="far fa-heart" />
                </div>
                <div
                  onClick={() => handleAddToCart({ doc_id, doc_data })}
                  className="btn-white add-to-cart-btn"
                >
                  Add to cart
                </div>
                <img
                  src={convert2SmallImg(doc_data.image_url[0])}
                  alt=""
                  srcSet
                  className="product-image"
                />
                <img
                  src={convert2SmallImg(doc_data.image_url[1])}
                  alt="Product"
                  className="product-image-hover"
                />
                <div className="brief-describe">
                  <span className="type">{doc_data.category[0]}</span>
                  <h3 className="brief-describe__titile-price">
                    {doc_data.name}
                    <br />${doc_data.price}
                  </h3>
                  <div className="star">
                    <RenderStar numStar={numStar} />

                    <span className="num-review">({numReview} Reviews)</span>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
      });
    return null;
  };
  return (
    <>
      <div className="trendy-product">
        <div className="grid wide">
          <h1 className="trendy-product__title">Trendy Products</h1>
          <ul className="trendy-product__menu">
            <li
              onClick={e => handleClickTab(e)}
              data-name="All"
              className={activeTab === "All" ? "active-li showing" : null}
            >
              ALL
            </li>
            <li
              onClick={e => handleClickTab(e)}
              data-name="Furniture"
              className={activeTab === "Furniture" ? "active-li showing" : null}
            >
              FURNITURE
            </li>
            <li
              onClick={e => handleClickTab(e)}
              data-name="Decoration"
              className={
                activeTab === "Decoration" ? "active-li showing" : null
              }
            >
              DECORATION
            </li>
            <li
              onClick={e => handleClickTab(e)}
              data-name="Lighting"
              className={activeTab === "Lighting" ? "active-li showing" : null}
            >
              LIGHTING
            </li>
          </ul>
          <div className="product-area">
            <div className="row">
              <ListTrendyProducts />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TrendyProduct;
