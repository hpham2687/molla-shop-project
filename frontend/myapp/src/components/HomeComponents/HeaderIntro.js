import React from "react";

const HeaderIntro = () => {
  return (
    <>
      <div className="intro">
        <div className="grid wide">
          <div className="row">
            <div className="col l-8 m-12 c-12">
              <section className="carousel" aria-label="Gallery">
                <ol className="carousel__viewport">
                  <li
                    id="carousel__slide1"
                    tabIndex={0}
                    className="carousel__slide slide1"
                  >
                    <div className="carousel__snapper">
                      <a href="#" className="carousel__prev">
                        Go to last slide
                      </a>
                      <a href="#carousel__slide2" className="carousel__next">
                        Go to next slide
                      </a>
                    </div>
                    <div className="carousel__text">
                      <div className="carousel__title">
                        <h3 className="subtitle">Outdoor Furniture</h3>
                        <h1 className="main-title">
                          Outdoor Dining <br /> Furniture
                        </h1>
                      </div>
                      <div className="carousel__btn btn-outline-white">
                        SHOP NOW
                        <i className="fa fa-caret-right" />
                      </div>
                    </div>
                  </li>
                  <li
                    id="carousel__slide2"
                    tabIndex={0}
                    className="carousel__slide slide2"
                  >
                    <div className="carousel__snapper" />
                    <a href="#carousel__slide1" className="carousel__prev">
                      Go to previous slide
                    </a>
                    <a href="#carousel__slide3" className="carousel__next">
                      Go to next slide
                    </a>
                    <div className="carousel__text">
                      <div className="carousel__title">
                        <h3 className="subtitle">Outdoor Furniture</h3>
                        <h1 className="main-title">
                          Outdoor Dining <br /> Furniture
                        </h1>
                      </div>
                      <div className="carousel__btn btn-outline-white">
                        SHOP NOW
                        <i className="fa fa-caret-right" />
                      </div>
                    </div>
                  </li>
                  <li
                    id="carousel__slide3"
                    tabIndex={0}
                    className="carousel__slide slide3"
                  >
                    <div className="carousel__snapper" />
                    <a href="#carousel__slide2" className="carousel__prev">
                      Go to previous slide
                    </a>
                    <a href="#carousel__slide4" className="carousel__next">
                      Go to next slide
                    </a>
                    <div className="carousel__text">
                      <div className="carousel__title">
                        <h3 className="subtitle">Outdoor Furniture</h3>
                        <h1 className="main-title">
                          Outdoor Dining <br /> Furniture
                        </h1>
                      </div>
                      <div className="carousel__btn btn-outline-white">
                        SHOP NOW
                        <i className="fa fa-caret-right" />
                      </div>
                    </div>
                  </li>
                </ol>
                <aside className="carousel__navigation">
                  <ol className="carousel__navigation-list">
                    <li className="carousel__navigation-item">
                      <a
                        href="#carousel__slide1"
                        className="carousel__navigation-button"
                      >
                        Go to slide 1
                      </a>
                    </li>
                    <li className="carousel__navigation-item">
                      <a
                        href="#carousel__slide2"
                        className="carousel__navigation-button"
                      >
                        Go to slide 2
                      </a>
                    </li>
                    <li className="carousel__navigation-item">
                      <a
                        href="#carousel__slide3"
                        className="carousel__navigation-button"
                      >
                        Go to slide 3
                      </a>
                    </li>
                  </ol>
                </aside>
              </section>
            </div>
            <div className="flex-intro-23 col l-4 m-0 c-12">
              <div className="intro-2">
                <div className="intro-overlay" />
                <div className="intro-2__text">
                  <div className="intro-2__title">
                    <h3 className="subtitle">Outdoor Furniture</h3>
                    <h1 className="main-title">
                      Outdoor Dining <br /> Furniture
                    </h1>
                  </div>
                  <div className="carousel__btn btn-outline-white intro-2-btn">
                    SHOP NOW
                    <i className="fa fa-caret-right" />
                  </div>
                </div>
              </div>
              <div className="intro-3">
                <div className="intro-overlay" />
                <div className="intro-2__text">
                  <div className="intro-2__title">
                    <h3 className="subtitle">New in</h3>
                    <h1 className="main-title">
                      Best Lighting <br /> Collection
                    </h1>
                  </div>
                  <div className="carousel__btn btn-outline-white intro-2-btn">
                    Discover Now
                    <i className="fa fa-caret-right" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col l-12 m-6 c-12">
              <div className="brand-ads">
                <ul>
                  <li>
                    <img
                      src="https://d-themes.com/react/molla/assets/images/brands/1.png"
                      alt=""
                      srcSet
                    />
                  </li>
                  <li>
                    <img
                      src="https://d-themes.com/react/molla/assets/images/brands/2.png"
                      alt=""
                      srcSet
                    />
                  </li>
                  <li>
                    <img
                      src="https://d-themes.com/react/molla/assets/images/brands/3.png"
                      alt=""
                      srcSet
                    />
                  </li>
                  <li>
                    <img
                      src="https://d-themes.com/react/molla/assets/images/brands/4.png"
                      alt=""
                      srcSet
                    />
                  </li>
                  <li>
                    <img
                      src="https://d-themes.com/react/molla/assets/images/brands/6.png"
                      alt=""
                      srcSet
                    />
                  </li>
                  <li>
                    <img
                      src="https://d-themes.com/react/molla/assets/images/brands/5.png"
                      alt=""
                      srcSet
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeaderIntro;
