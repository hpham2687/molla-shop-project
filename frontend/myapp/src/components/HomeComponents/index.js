import React, { useContext } from "react";
import HeaderIntro from "./HeaderIntro";
import TrendyProduct from "./TrendyProduct";
import RecentArrival from "./RecentArrival";
import OurFeatures from "./OurFeatures";
import ListBlog from "./ListBlog";
import ShopByCategory from "./ShopByCategory";

const MainHome = props => {
  return (
    <>
      <HeaderIntro />
      <TrendyProduct {...props} />
      <ShopByCategory />
      <RecentArrival {...props} />
      <OurFeatures />
      <ListBlog />

      {/* sign up banner */}
      <div className="sign-up-banner">
        <div className="sign-up-banner__wrapper">
          <div className="sign-up-banner__left">
            <div className="sign-up-banner__left-title">
              Sign Up &amp; Get 10% Off
            </div>
            <p className="sign-up-banner__left-subtitle">
              Molla presents the best in interior design
            </p>
          </div>
          <div className="sign-up-banner__btn">
            <div className="carousel__btn btn-outline-white">
              SHOP NOW
              <i className="fa fa-caret-right" />
            </div>
          </div>
        </div>
      </div>
      {/* sign up banner */}
    </>
  );
};

export default MainHome;
