import React from "react";

const ShopByCategory = () => {
  return (
    <>
      <div className="trendy-product">
        <div className="grid wide">
          <h1 className="trendy-product__title">Shop by Categories</h1>
          <div className="product-by-cate-area">
            <div className="row">
              <div className="col l-4 m-6 c-12">
                <div className="product-by-cate-area-item">
                  <div className="intro-overlay" />
                  <h3 className="product-by-cate-area-item__title">Outdoor</h3>
                  <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                    SHOP NOW
                    <i className="fa fa-caret-right" />
                  </div>
                  <div className="product-by-cate-area-item-background">
                    <img
                      src="https://d-themes.com/react/molla/assets/images/banners/home/banner-1.jpg"
                      alt=""
                      srcSet
                    />
                  </div>
                </div>
              </div>
              <div className="col l-4 m-6 c-12">
                <div className="col-list-item">
                  <div className="product-by-cate-area-item">
                    <div className="intro-overlay" />
                    <h3 className="product-by-cate-area-item__title">
                      Furniture and Design
                    </h3>
                    <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                      SHOP NOW
                      <i className="fa fa-caret-right" />
                    </div>
                    <div className="product-by-cate-area-item-background">
                      <img
                        src="https://d-themes.com/react/molla/assets/images/banners/home/banner-2.jpg"
                        alt=""
                        srcSet
                      />
                    </div>
                  </div>
                  <div className="product-by-cate-area-item">
                    <div className="intro-overlay" />
                    <h3 className="product-by-cate-area-item__title">
                      Kitchen &amp; Utensil
                    </h3>
                    <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                      SHOP NOW
                      <i className="fa fa-caret-right" />
                    </div>
                    <div className="product-by-cate-area-item-background">
                      <img
                        src="https://d-themes.com/react/molla/assets/images/banners/home/banner-2.jpg"
                        alt=""
                        srcSet
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col l-4 m-6 c-12">
                <div className="product-by-cate-area-item">
                  <div className="intro-overlay" />
                  <h3 className="product-by-cate-area-item__title">Lighting</h3>
                  <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                    SHOP NOW
                    <i className="fa fa-caret-right" />
                  </div>
                  <div className="product-by-cate-area-item-background">
                    <img
                      src="https://d-themes.com/react/molla/assets/images/banners/home/banner-1.jpg"
                      alt=""
                      srcSet
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ShopByCategory;
