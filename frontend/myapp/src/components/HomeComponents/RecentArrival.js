import React, { useState } from "react";
import { convert2SmallImg, filterProduct } from "../../helpers";
import { showNotification } from "../Notification";

const RecentArrival = props => {
  const activeTab = props.uiContexts.ui.recentArrivalProductTab;

  let [numProduct2Show, setNumProduct2Show] = useState(3);
  let [loading, setLoading] = useState(false);

  const handleLoadingMore = () => {
    setLoading(true)
    console.log(numProduct2Show);
    if (numProduct2Show+4 >props.productDataContext.productData.length) setNumProduct2Show(props.productDataContext.productData.length);
    setNumProduct2Show(prevState => prevState +4)
    setInterval(()=>{
      setLoading(false)

    },500)
  }
  const handleClickTab = e => {
    console.log(e.currentTarget.dataset.name);
    props.uiContexts.UiDispatch({
      type: "SET_RECENT_ARRIVAL_PRODUCT_TAB",
      payload: e.currentTarget.dataset.name
    });
  };
  const RenderStar = ({ numStar }) => {
    let a = [];
    for (var i = 0; i < 5; i++) {
      if (i <= numStar) a.push(<i className="fa fa-star active-star" />);
      else a.push(<i className="fa fa-star" />);
    }
    return a;
  };

  const ListRecentArrivalProducts = () => {
    if (props.productDataContext.productData.length > 0)
      return filterProduct(props.productDataContext.productData, [
        activeTab,        
      ]).map((value, index) => {
           if (index > numProduct2Show) return null; // chỉ hiện thị 4 phần tử
        const { doc_id } = value;
        const { doc_data } = value;
        const numReview = doc_data.reviews.length || 0;
        const numStar = Math.floor(
          doc_data.reviews.reduce(function(acc, val) {
            return acc + val.count || 2;
          }, 0) / numReview
        );

        //   console.log(numStar);
        const handleAddToCart = ({ doc_id, doc_data }) => {
          // console.log(doc_id, doc_data)
          props.cartContexts.cartDispatch({
            type: "SET",
            payload: { doc_id, doc_data }
          });
          showNotification("success", "Item added to card!");
        };
        return (
          <>
            <div className="col l-3 m-4 c-6">
              <div className="product-item">
                <div className="product-label product-label--red">NEW</div>
                <div className="add-to-wishlish-icon">
                  <i className="far fa-heart" />
                </div>
                <div
                  onClick={() => handleAddToCart({ doc_id, doc_data })}
                  className="btn-white add-to-cart-btn"
                >
                  Add to cart
                </div>
                <img
                  src={convert2SmallImg(doc_data.image_url[0])}
                  alt=""
                  srcSet
                  className="product-image"
                />
                <img
                  src={convert2SmallImg(doc_data.image_url[1])}
                  alt="Product"
                  className="product-image-hover"
                />
                <div className="brief-describe">
                  <span className="type">{doc_data.category[0]}</span>
                  <h3 className="brief-describe__titile-price">
                    {doc_data.name}
                    <br />${doc_data.price}
                  </h3>
                  <div className="star">
                    <RenderStar numStar={numStar} />

                    <span className="num-review">({numReview} Reviews)</span>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
      });
    return null;
  };

  return (
    <>
      <div className="trendy-product">
        <div className="grid wide">
          <h1 className="trendy-product__title">Recent Arrivals</h1>
          <ul className="trendy-product__menu">
            <li
              onClick={e => handleClickTab(e)}
              data-name="All"
              className={activeTab === "All" ? "active-li showing" : null}
            >
              ALL
            </li>
            <li
              onClick={e => handleClickTab(e)}
              className={activeTab === "Furniture" ? "active-li showing" : null}
              data-name="Furniture"
            >
              FURNITURE
            </li>
            <li
              onClick={e => handleClickTab(e)}
              className={
                activeTab === "Decoration" ? "active-li showing" : null
              }
              data-name="Decoration"
            >
              DECORATION
            </li>
            <li
              onClick={e => handleClickTab(e)}
              className={activeTab === "Lighting" ? "active-li showing" : null}
              data-name="Lighting"
            >
              LIGHTING
            </li>
          </ul>
          <div className="product-area">
            <div className="row">
              <ListRecentArrivalProducts />
            </div>
          </div>
          <div className="more-container">
            <div onClick={handleLoadingMore} className="load-more-btn btn-white">
              LOAD MORE ITEMS
              {!loading? <i className="fa fa-long-arrow-down" /> : <i className="fas fa-spinner" />}
              
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default RecentArrival;
