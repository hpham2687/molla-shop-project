import React from "react";

const Footer = () => {
  return (
    <>
      <div className="footer">
        <div className="grid wide">
          <div className="footer-menu">
            <div className="row">
              <div className="col l-3 m-6 c-12">
                <div className="footer-item">
                  <div className="footer-item__heading">
                    <div className="logo">
                      <img
                        src="https://d-themes.com/react/molla/assets/images/logo-footer.png"
                        alt=""
                        srcSet
                      />
                    </div>
                  </div>
                  <div className="footer-description">
                    Praesent dapibus, neque id cursus ucibus, tortor neque
                    egestas augue, eu vulputate magna eros eu erat.
                  </div>
                  <div className="social-icons" />
                </div>
              </div>
              <div className="col l-3 m-6 c-12">
                <div className="footer-item">
                  <div className="footer-item__heading">Useful Links</div>
                  <div className="footer-description">
                    <ul>
                      <li>About Molla</li>
                      <li>How to shop on Molla</li>
                      <li>FAQ</li>
                      <li>Contact us</li>
                      <li>Login</li>
                    </ul>
                  </div>
                  <div className="social-icons" />
                </div>
              </div>
              <div className="col l-3 m-6 c-12">
                <div className="footer-item">
                  <div className="footer-item__heading">Customer Service</div>
                  <div className="footer-description">
                    <ul>
                      <li>Payment Methods</li>
                      <li>Money-back guarantee!</li>
                      <li>Returns</li>
                      <li>Shipping</li>
                      <li>Terms and conditions</li>
                      <li>Privacy Policy</li>
                    </ul>
                  </div>
                  <div className="social-icons" />
                </div>
              </div>
              <div className="col l-3 m-6 c-12">
                <div className="footer-item">
                  <div className="footer-item__heading">My Account</div>
                  <div className="footer-description">
                    <ul>
                      <li>Sign in</li>
                      <li>View cart</li>
                      <li>My wishlist</li>
                      <li>Track my order</li>
                      <li>Help</li>
                    </ul>
                  </div>
                  <div className="social-icons" />
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="footer-bottom">
              Copyright © 2020 Molla Store. All Rights Reserved.
              <img
                src="https://d-themes.com/react/molla/assets/images/payments.png"
                alt=""
                srcSet
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
