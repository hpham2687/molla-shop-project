import React, { useContext, useEffect } from "react";
import $ from "jquery";
import { convert2SmallImg } from "../helpers";
import { showNotification } from "./../components/Notification";
import { Link } from "react-router-dom";

const Header = props => {
  const { authUser } = props.userContexts.user;

  const currentInCart =
    props.cartContexts.cart.reduce(function(acc, val) {
      return acc + val.count;
    }, 0) || 0;

  const listDropdownItems = props.cartContexts.cart;
  const RenderListDropdownItems = () => {
    if (listDropdownItems.length > 0)
      return listDropdownItems.map((value, index) => {
        const { count, doc_data, doc_id } = value;
        return (
          <>
            <li key={index}>
              <div className="dropdown-cart-items-left">
                <h3>{doc_data.name}</h3>
                <span className="dropdown-cart-items-left__amount">
                  {count}x ${doc_data.price}
                </span>
              </div>
              <div className="dropdown-cart-items-right">
                <img src={convert2SmallImg(doc_data.image_url[0])} />
                <span
                  onClick={() =>
                    props.cartContexts.cartDispatch({
                      type: "DELETE",
                      payload: doc_id
                    })
                  }
                  className="dropdown-cart-items-left__close"
                >
                  X
                </span>
              </div>
            </li>
          </>
        );
      });
    return "no item";
  };

  //  console.log(authUser)
  const handleClickLogin = () => {
    if (!authUser)
      props.uiContexts.UiDispatch({ type: "SET_MODAL_STATUS", status: true });
    else {
      // click log out
      props.firebase
        .doSignOut()
        .then(() => {
          showNotification("success", "Log out successfully!");

          //  props.userContexts.userDispatch({type:"SET", payload: null})
          console.log("Signed Out");
        })
        .catch(error => {
          showNotification("success", "Log out error!"+ error);

        });
    }
  };
  useEffect(() => {
    $(".menu-li").hover(
      function() {
        $(".shop-menu-dropdown").css("display", "flex");
      },
      function() {
        $(".shop-menu-dropdown").css("display", "none");
      }
    );

    $("#view-cart-icon").hover(
      function() {
        $(".dropdown-cart-items").css("display", "block");
      },
      function() {
        $(".dropdown-cart-items").css("display", "none");
      }
    );
  }, []);
  return (
    <>
      <header className="header">
        <div className="grid wide">
          <div className="header__top">
            <div className="header__top-left">
              <div className="dropdown-menu">
                <a href="/">
                  USD
                  <i className="fa fa-angle-down" />
                </a>
                <div className="dropdown-menu__list">
                  <ul>
                    <li>
                      <a href="/">Eur</a>
                    </li>
                    <li>
                      <a href="/">Usd</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="dropdown-menu">
                <a href="/">
                  ENG
                  <i className="fa fa-angle-down" />
                </a>
                <div className="dropdown-menu__list">
                  <ul>
                    <li>
                      <a href="/">VIE</a>
                    </li>
                    <li>
                      <a href="/">ENG</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="header__top-right">
              <ul>
                <li>
                  <i className="fa fa-phone" />
                  CALL: +0123 456 789
                </li>
                <li>
                  <i className="far fa-heart" />
                  MY WISHLIST (0)
                </li>
                <li>ABOUT US</li>
                <li onClick={() => handleClickLogin()}>
                  <i className="fa fa-user" />
                  {authUser ? "LOG OUT" : "LOGIN"}
                </li>
              </ul>
            </div>
          </div>
          <div className="header__bottom">
            <div className="header__bottom-left">
              <div className="bar-mobile moblie">
                <i className="fa fa-bars" />
              </div>
              <div className="logo">
                <Link to="/">
                <img
                  src="https://d-themes.com/react/molla/assets/images/logo.png"
                  alt=""
                />
                </Link>
              </div>
              <ul className="menu-li">
                <li className="active-li">
                  HOME
                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  SHOP
                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  PRODUCT
                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  PAGES
                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  BLOG
                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  ELEMENTS
                  <i className="fa fa-angle-down" />
                </li>
                <div className="shop-menu-dropdown">
                  <div className="wrapper12">
                    <div className="col1">
                      <div className="menu-dropdown__title">
                        SHOP WITH SIDEBAR
                      </div>
                      <ul>
                        <li>Shop List</li>
                        <li>Shop Grid 2 Columns</li>
                        <li>Shop Grid 3 Columns</li>
                        <li>Shop Grid 4 Columns</li>
                      </ul>
                      <div className="menu-dropdown__title">
                        SHOP NO SIDEBAR
                      </div>
                      <ul>
                        <li>Shop Boxed No Sidebar</li>
                        <li>Shop Fullwidth No Sidebar</li>
                      </ul>
                    </div>
                    <div className="col2">
                      <div className="menu-dropdown__title">
                        SHOP WITH SIDEBAR
                      </div>
                      <ul>
                        <li>Shop List</li>
                        <li>Shop Grid 2 Columns</li>
                        <li>Shop Grid 3 Columns</li>
                        <li>Shop Grid 4 Columns</li>
                      </ul>
                      <div className="menu-dropdown__title">
                        SHOP NO SIDEBAR
                      </div>
                      <ul>
                        <li>Shop Boxed No Sidebar</li>
                        <li>Shop Fullwidth No Sidebar</li>
                      </ul>
                    </div>
                  </div>
                  <div className="col3">
                    <img
                      src="https://d-themes.com/react/molla/assets/images/menu/banner-1.jpg"
                      alt="Banner"
                    />
                  </div>
                </div>
              </ul>
            </div>
            <div className="header__bottom-right">
              <ul>
                <li className="li-search-group">
                  <label htmlFor="check-search" className="check-search">
                    <i className="far fa-search" />
                  </label>
                  <input
                    type="checkbox"
                    id="check-search"
                    name="check-search"
                  />
                  <input
                    type="search"
                    className="input-search"
                    name="q"
                    id="q"
                    placeholder="Search in..."
                    required
                  />
                </li>
                <li id="view-cart-icon">
                  <Link to="/checkout">
                  <i className="far fa-shopping-cart" />
                  <span className="cart-number">{currentInCart}</span>
                  </Link>
                  <div className="dropdown-cart-items">
                    <ul>
                      <RenderListDropdownItems />
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
