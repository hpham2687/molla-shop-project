import React, { useContext, Component, useState } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { showNotification } from "./../components/Notification";


const ModalMolla = props => {
  return (
    <>
      {props.uiContexts.ui.isShowModalAuth ? <SignInForm {...props} /> : null}
    </>
  );
};

const INITIAL_STATE = {
  email: "",
  password: "",
  error: null,
  loading: false,
  currentTab: "login"
};
class SignInFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
    this.firebase = { ...this.props.firebase };
    this.userContexts = { ...this.props.userContexts };
    this.uiContexts = { ...this.props.uiContexts };

    this.state = {
      currentTab: "login"
    };
  }

  onSubmitRegister = event => {
    event.preventDefault();
    this.setState({ loading: true });
    const { email, password } = this.state;

    this.firebase
      .createUserWithEmailAndPassword(email, password)
      .then(authUser => {
        console.log(authUser);
        showNotification("success", "Register successfully!");

     //   this.uiContexts.UiDispatch({ type: "SET_MODAL_STATUS", status: false });
      })
      .catch(error => {
        this.setState({ error });
        showNotification("success", "Register error!"+ error);

        // this.props.history.push('/error');
        this.setState({ loading: false });
      });
  };

  onSubmitLogin = event => {
    event.preventDefault();
    this.setState({ loading: true });
    const { email, password } = this.state;

    this.firebase
      .signInWithEmailAndPassword(email, password)
      .then(authUser => {
        console.log(authUser);
        showNotification("success", "Login successfully!");

        // this.userContexts.userDispatch({type:"SET", payload: authUser})
        // this.uiContexts.UiDispatch({type:'SET_MODAL_STATUS', status: false})
      })
      .catch(error => {
        this.setState({ error });
        showNotification("success", "Login error!"+ error);

        this.setState({ loading: false });
      });
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClickTab = event => {
    let nameTabClicked = event.currentTarget.dataset.name;
    if (nameTabClicked == "loginTab") {
      console.log("loginTab");

      this.setState({ currentTab: "login" });
    } else if (nameTabClicked == "registerTab") {
      console.log("registerTab");

      this.setState({ currentTab: "register" });
    }
  };
  handleCloseModal = () => {
    this.uiContexts.UiDispatch({ type: "SET_MODAL_STATUS", status: false });
  };

  RenderLoginTab = () => {
    const { email, password, loading } = this.state;

    return (
      <>
        <div className="form-group">
          <div className="label-input">Username or email address *</div>
          <input
            type="text"
            value={email}
            onChange={this.onChange}
            className="form-group__input"
            name="email"
            required
          />
        </div>
        <div className="form-group">
          <div className="label-input">Password *</div>
          <input
            type="password"
            value={password}
            onChange={this.onChange}
            name="password"
            className="form-group__input"
            required
          />
        </div>
        <div className="login-row">
          <div
            onClick={e => this.onSubmitLogin(e)}
            className="btn-outline-orange"
          >
            SHOP NOW
            {loading ? (
              <i class="fas fa-spinner"></i>
            ) : (
              <i className="fa fa-caret-right" />
            )}
          </div>
          <div className="remember-me">
            <input type="checkbox" id="signin-remember" />
            <label htmlFor="signin-remember">Remember Me</label>
          </div>
          <div className="forgot-password">Forgot Your Password?</div>
        </div>
      </>
    );
  };

  RenderRegisterTab = () => {
    const { email, password, loading } = this.state;

    return (
      <>
        <div className="form-group">
          <div className="label-input">Your email address *</div>
          <input
            type="text"
            value={email}
            onChange={this.onChange}
            className="form-group__input"
            name="email"
            required
          />
        </div>
        <div className="form-group">
          <div className="label-input">Password *</div>
          <input
            type="password"
            value={password}
            onChange={this.onChange}
            name="password"
            className="form-group__input"
            required
          />
        </div>
        <div className="login-row">
          <div
            onClick={e => this.onSubmitRegister(e)}
            className="btn-outline-orange"
          >
            SIGN UP
            {loading ? (
              <i class="fas fa-spinner"></i>
            ) : (
              <i className="fa fa-caret-right" />
            )}
          </div>
          <div className="remember-me">
            <input type="checkbox" id="signin-remember" />
            <label htmlFor="signin-remember">
              I agree to the privacy policy *
            </label>
          </div>
        </div>
      </>
    );
  };
  render() {
    const { email, password, error, loading, currentTab } = this.state;

    const isInvalid = password === "" || email === "";

    const RenderForm =
      currentTab === "login" ? this.RenderLoginTab : this.RenderRegisterTab;

    return (
      <>
        <div className="modal-molla">
          <div className="modal-auth">
            <div className="modal-auth__tab">
              <span
                data-name="loginTab"
                onClick={e => this.handleClickTab(e)}
                className={
                  currentTab === "login" ? "modal-auth__tab--active" : null
                }
              >
                Sign In
              </span>
              <span
                data-name="registerTab"
                onClick={e => this.handleClickTab(e)}
                className={
                  currentTab === "register" ? "modal-auth__tab--active" : null
                }
              >
                Register
              </span>
            </div>
            <RenderForm />
            {error && <p>{error.message}</p>}
          </div>
          <div
            onClick={() => this.handleCloseModal()}
            className="modal-molla__overlay"
          ></div>
        </div>
      </>
    );
  }
}

const SignInForm = compose(withRouter)(SignInFormBase);

export default ModalMolla;
