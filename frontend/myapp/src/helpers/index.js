export const convert2SmallImg = url => {
  return `${url.substring(0, url.length - 4)}-300x300.jpg`;
};


export const getTrendyProducts = (products) => {
  console.log(products);
}

export const filterProduct = (data, arrayCondition) => {
  const activeTab = arrayCondition[0];
  const isFilterActiveTab = arrayCondition[0] !== null;
  return data
    .filter(value => {
      // lọc qua từng phần tử
      if (!isFilterActiveTab) {
        // nếu có lọc qua active tab,
        console.log("not isFilterActiveTab");
        return 1;
      }
      if (activeTab == "All") return 1; // nếu tab là all, cho qua

      // check trong từng categrory của phần tử, nếu == với activeTab thì cho qua
      return value.doc_data.category.includes(activeTab);
    })
    .filter(value => {
      // lọc trendy
      if (!arrayCondition.includes("trendy")) {
        return 1;
      }
      if (value.doc_data.trendy) return 1;
      return 0;
    })
    .filter(value => {
      // lọc theo time
      if (!arrayCondition.includes("newArrival")) {
        return 1;
      }
      if (value.doc_data.dateArrvial > 1598798141) return 1;
      return 0;
    });
};
